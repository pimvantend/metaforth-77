# metaforth-77

A revival of Observatory Metaforth as created by Jan Vermue and Hans
Nieuwenhuijzen in 1977/1978. Metaforth is a program written in forth to
create another forth system on an hp2100.

Historical and practical information can be found in
[the project wiki](https://gitlab.com/pimvantend/metaforth-77/wikis/home).

