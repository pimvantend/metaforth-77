#!/usr/bin/env python
invoer=open('metaforth.hp2100','r')
uitvoer=open('blocks.fs','r+')
regelinblok=0
vorigeblok=0
for regel in invoer:
  regellijst=regel.split(' ')
  if regel.startswith('( ') and r'/' in regel:
    print regel
    regelinblok=0
    bloknummer=int(regellijst[1])
    nmin1=bloknummer-1
    if bloknummer<vorigeblok:
      break
    else:
      vorigeblok=bloknummer
#  print
#  adres=0x2c00+bloknummer*0x400+regelinblok*64
  adres=0x3000+0x400*(nmin1%12)+0x6000*int(nmin1/12)+regelinblok*64
  uitvoer.seek(adres)
#  print bloknummer,hex(adres)
  regel1=regel[:-1].upper()
  if len(regel1)%2==1:
    regel1+=' '
  uitregel=''
  for positie in range(0,len(regel1),2):
    uitregel+=regel1[positie+1]
    uitregel+=regel1[positie]
  for positie in range(len(regel1),64):
    uitregel+=' '
  uitvoer.write(uitregel)
#  print uitregel+'$'
  regelinblok+=1
